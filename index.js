const chalk = require('chalk')
const log = console.log
const query = require('./lib/query')
const figlet = require('figlet')
const inquirer = require('inquirer')
var program = require('commander')
var pkginfo = require('pkginfo')(module, 'version');
const Configstore = require('configstore')
const conf = new Configstore(pkginfo.name)

function infiniteQueryLoop(gql) {
  query.doQuery(gql).then(() => {
    infiniteQueryLoop(gql)
  }).catch((error) => {
    console.log(chalk.red('Exiting due to error'))
  })
}

function infiniteMutationLoop(gql) {
  query.doMutation(gql).then(() => {
    infiniteMutationLoop(gql)
  }).catch((error) => {
    console.log(chalk.red('Exiting due to error'))
  })
}

function infiniteApiLoop(gql, delay) {
  query.doApi(gql, delay).then(() => {
    infiniteApiLoop(gql, delay)
  }).catch((error) => {
    console.log(chalk.red('Exiting due to error'))
  })
}


// All
function infiniteAutoLoop() {
  query.doAllQueries().then(() => {
    query.doAllMutations().then(() => {
      infiniteAutoLoop()
    })
  })
}
// All
function infiniteAutoApiLoop() {
  query.doAllApi().then(() => {
    infiniteAutoApiLoop()
  })
}

function run() {
  program
    .command('api <gql>')
    .alias('a')
    .description('Do GQL queries specific to the protobuf')
    .option('-r, --repeat', 'Repeat query infinitely')
    .option('-d, --delay [type]', 'Add a delay in ms')
    .action(function(gql, args) {
      if (args.repeat) {
        log(chalk.cyan(`Querying: ${gql} infinitely`))
        infiniteApiLoop(gql, args.delay)
      } else {
        log(chalk.cyan(`Querying: ${gql} once`))
        query.doApi(gql).catch(error => {
          console.log(chalk.red('Failed due to error.'))
        })
      }
    })

  program
    .command('query <gql>')
    .alias('q')
    .description('Query something')
    .option('-r, --repeat', 'Repeat query infinitely')
    .action(function(gql, args) {
      if (args.repeat) {
        log(chalk.cyan(`Querying: ${gql} infinitely`))
        infiniteQueryLoop(gql)
      } else {
        log(chalk.cyan(`Querying: ${gql} once`))
        query.doQuery(gql).catch(error => {
          console.log(chalk.red('Failed due to error.'))
        })
      }
    })

  program
    .command('mutation <gql>')
    .alias('m')
    .description('Mutate something')
    .option('-r, --repeat', 'Repeat query infinitely')
    .action(function(gql, args) {
      if (args.repeat) {
        log(chalk.cyan(`Performing Mutation: ${gql} infinitely`))
        infiniteMutationLoop(gql)
      } else {
        log(chalk.cyan(`Performing Mutation: ${gql} once`))
        query.doMutation(gql)
      }
    })

  program
    .command('auto')
    .alias('a')
    .description('Automatically run all queries and mutations')
    .option('-r, --repeat', 'Repeat infinitely')
    .action(function(args) {
      if (args.repeat) {
        log(chalk.cyan(`Performing automatic infinitely`))
        infiniteAutoLoop()
      } else {
        log(chalk.cyan(`Performing automatic once`))
        query.doAllQueries()
        query.doAllMutations()
      }
    })

  program
    .command('*')
    .action(function() {
      program.help()
    })

  program
    .version(pkginfo.version)
    .parse(process.argv)

  if (program.args.length === 0){
    ayypex(true)
  }
}

function ayypex(start) {
  if (start) {
    log(chalk.yellow(figlet.textSync('AyyPex', { font: 'Alligator', horizontalLayout: 'controlled smushing' })))
  }
  inquirer
    .prompt([{
      type: 'list',
      name: 'action',
      message: 'Select an action',
      choices: [
        'API',
        'API - All - Once',
        'API - All - Infinite',
        new inquirer.Separator(),
        'Query',
        'Mutation',
        'Automatic - Once',
        'Automatic - Infinite',
        new inquirer.Separator(),
        'Set Authentication',
        'Set Endpoint',
        new inquirer.Separator(),
        'Exit'
      ],
      pageSize: 15
  }]).then(answers => {
    switch(answers.action) {
      case 'API':
        menuApi()
        break
      case 'API - All - Once':
        menuApiAuto(false)
        break
      case 'API - All - Infinite':
        menuApiAuto(true)
        break
      case 'Query':
        menuQuery()
        break
      case 'Mutation':
        menuMutation()
        break
      case 'Automatic - Once':
        menuAuto(false)
        break
      case 'Automatic - Infinite':
        menuAuto(true)
        break
      case 'Set Authentication':
        menuAuth()
        break
      case 'Set Endpoint':
        menuEndpoint()
        break
      case 'Exit':
        console.log('Exiting...')
        break
      default:
        console.log('Unknown action selected')
        break
    }
  })  
}

run()

  function menuApi() {
    inquirer
    .prompt([{
      type: 'list',
      name: 'gqlKey',
      message: 'Select a query',
      choices: Object.keys(query.apiMap),
      pageSize: 12
    }]).then(answers => {
      query.doApi(answers.gqlKey).then(() => {
        console.log('\n')
        ayypex()
      }, (error) => {
        console.log('\n')
        ayypex()
      })
    })
  }

  function menuApiAuto(infinite) {
    if (infinite) {
      infiniteAutoApiLoop()
    } else {
      query.doAllApi()
    }
  }

  function menuQuery() {
    inquirer
    .prompt([{
      type: 'list',
      name: 'gqlKey',
      message: 'Select a query',
      choices: Object.keys(query.queryMap),
      pageSize: 12
    }]).then(answers => {
      query.doQuery(answers.gqlKey).then(() => {
        console.log('\n')
        ayypex()
      }, (error) => {
        console.log('\n')
        ayypex()
      })
    })
  }

  function menuMutation() {
    inquirer
    .prompt([{
      type: 'list',
      name: 'gqlKey',
      message: 'Select a mutation',
      choices: Object.keys(query.mutationMap),
      pageSize: 12
    }]).then(answers => {
      query.doMutation(answers.gqlKey).then(() => {
        console.log('\n')
        ayypex()
      }, (error) => {
        console.log('\n')
        ayypex()
      })
    })
  }

  function menuAuto(infinite) {
    if (infinite) {
      infiniteAutoLoop()
    } else {
      query.doAllQueries()
      query.doAllMutations()
    }
  }

  function menuAuth() {
    if(conf.get('jwt')) {
      console.log(chalk.green('JWT: ' + conf.get('jwt')))
    }
    inquirer
    .prompt([{
      type: 'list',
      name: 'authType',
      message: 'Choose a method to authenticate',
      choices: ['Username & Password', 'Pre-authorized JWT']
    }]).then(answers => {
      if(answers.authType === 'Pre-authorized JWT') {
        inquirer.prompt([{
          type: 'input',
          name: 'jwt',
          message: 'Enter the pre-authorized jwt',
        }]).then(answers => {
          conf.set('jwt', answers.jwt)
          console.log('Successfully set JWT')
          query.updateAuthHeader(conf.get('jwt'))
          ayypex()
        })
      } else {
        console.log(answers.authType + ' is coming soon!')
        ayypex()
      }
    })
  }

  function menuEndpoint() {
    if(conf.get('gql_endpoint')) {
      console.log(chalk.green('Current GQL Endpoint: ' + conf.get('gql_endpoint')))
    }
    inquirer
      .prompt([{
        type: 'input',
        name: 'endpoint',
        message: 'Enter a new GQL endpoint'
      }]).then(answers => {
        conf.set('gql_endpoint', answers.endpoint)
        console.log('Successfully set new endpoint to ' + answers.endpoint)
        query.updateGraphqlEndpoint(conf.get('gql_endpoint'))
        ayypex()
      })
  }