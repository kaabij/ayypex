const graphql = require('graphql-request')
const chalk = require('chalk')
const figures = require('figures')
const log = console.log
var pkginfo = require('pkginfo')(module, 'version');
const Configstore = require('configstore')
const conf = new Configstore(pkginfo.name, {gql_endpoint: 'http://localhost:3000/query'})

const api_module = `{
  module(serviceName: "acquire", moduleName: "acquire-genicam") {
    moduleName
    serviceName
  }
}`

const api_modules = `{
  modules() {
    edges {
      node {
        moduleName
        serviceName
      }
    }
  }
}`

const api_module_configuration = `{
  moduleConfiguration(serviceName: "acquire", moduleName: "acquire-genicam") {
    id
  }
}`

const api_module_status = `{
  moduleStatus(serviceName: "acquire", moduleName: "acquire-genicam") {
    enabled
  }
}`

const api_module_settings = `{
  moduleSettings(serviceName: "acquire", moduleName: "acquire-genicam") {
    key
    value
  }
}`

const api_module_job = `{
  moduleJob(serviceName: "acquire", moduleName: "acquire-genicam", id: "1234") {
    id
  }
}`

const api_module_jobs = `{
  moduleJobs(serviceName: "acquire", moduleName: "acquire-genicam") {
    edges {
      node {
        id
      }
    }
  }
}`

const api_module_property = `{
  moduleProperty(serviceName: "state", moduleName: "state-ui", propertyName: "timer") {
    key
    value
  }
}`

const api_module_properties = `{
  moduleProperties(serviceName: "state", moduleName: "state-cent", propertyNames: ["image1-uri", "image1-name"]) {
    key
    value
  }
}`

const config_query = `{
  configuration(id: "5c1189d2ac625a019f19a630", namespace: "present") {
    id
  }
}`

const configs_query = `{
  configurations() {
    edges {
      node {
        id
      }
    }
  }
}`

const user_query = `{
  user(email: "jon.liu@coanda.ca") {
    id
  }
}`

const module_query = `{
  module(serviceName: "acquire", moduleName: "acquire-genicam") {
    moduleName
    serviceName
    state
  }
}`

const modules_query = `{
  modules() {
    edges {
      node {
        moduleName
        serviceName
        state
      }
    }
  }
}`

const module_configuration_query = `{
  module(serviceName: "acquire", moduleName: "acquire-genicam") {
    configuration {
      id
    }
  }
}`

const module_status_query = `{
  module(serviceName: "acquire", moduleName: "acquire-genicam") {
    status {
      enabled
      loaded
      active
    }
  }
}`

const module_job_query = `{
  module(serviceName: "acquire", moduleName: "acquire-genicam") {
    jobs(id: "1234") {
      edges {
        node {
          id
          status
        }
      }
    }
  }
}`

const module_jobs_query = `{
  module(serviceName: "acquire", moduleName: "acquire-genicam") {
    jobs {
      edges {
        node {
          id
          status
        }
      }
    }
  }
}`

const module_active_job_query = `{
  module(serviceName: "acquire", id: "acquire-genicam") {
    jobs(filter: "active") {
      edges {
        node {
          id
          status
        }
      }
    }
  }
}`

const experiment_query = `{
  experiment(id: "1234") {
    id
  }
}`

const submit_job_mutation = `mutation {
  submitJob(serviceName: "acquire", moduleName: "acquire-genicam", action: "connect") {
    id
  }
}`

const cancel_job_mutation = `mutation {
  cancelJob(serviceName: "acquire", moduleName: "acquire-genicam", jobId: "1234") {
    id
  }
}`

const queryMap = {
  configuration: config_query,
  configurations: configs_query,
  module: module_query,
  modules: modules_query,
  'module-configuration': module_configuration_query,
  'module-status': module_status_query,
  'module-job': module_job_query,
  'module-jobs': module_jobs_query,
  'module-active-job': module_active_job_query,
  user: user_query,
  experiment: experiment_query
}

const apiMap = {
  "api-module": api_module,
  "api-modules": api_modules,
  "api-module-configuration": api_module_configuration,
  "api-module-status": api_module_status,
  "api-module-settings": api_module_settings,
  "api-module-job": api_module_job,
  "api-module-jobs": api_module_jobs,
  "api-module-property": api_module_property,
  "api-module-properties": api_module_properties
}

const mutationMap = {
  'submit-job': submit_job_mutation,
  'cancel-job': cancel_job_mutation
}

const graphqlClient = new graphql.GraphQLClient(conf.get('gql_endpoint'), {
  headers: {
    authorization: 'Bearer ' + conf.get('jwt')
  }
})

exports.updateGraphqlEndpoint = (endpoint) => {
  graphqlClient.url = endpoint
}
exports.updateAuthHeader = (header) => {
  graphqlClient.setHeader('authorization', `Bearer ` + header)
}
exports.queryMap = queryMap
exports.mutationMap = mutationMap
exports.apiMap = apiMap

exports.doApi = (name, delay) => new Promise((resolve, reject) => {
  graphqlClient.request(apiMap[name]).then(data => {
    console.log(chalk.green(figures.tick), name)
    console.log(data)
    setTimeout(() => {
      resolve(data)
    }, delay)
  }).catch((error) => {
    console.log(chalk.red(figures.cross), name)
    console.log(chalk.bgRed(error))
    reject(error)
  })
})

exports.doAllApi = () => {
  var promises = []
  for (let key in apiMap) {
    let request = graphqlClient.request(apiMap[key]).then(data => {
      console.log(chalk.green(figures.tick), key)
    }).catch((error) => {
      console.log(chalk.red(figures.cross), key)
    })
    promises.push(request)
  }
  return Promise.all(promises)
}

exports.doQuery = (queryName) => new Promise((resolve, reject) => {
  graphqlClient.request(queryMap[queryName]).then(data => {
    console.log(chalk.green(figures.tick), queryName)
    console.log(data)
    resolve(data)
  }).catch((error) => {
    console.log(chalk.red(figures.cross), queryName)
    console.log(chalk.bgRed(error))
    reject(error)
  })
})

exports.doAllQueries = () => {
  var promises = []
  for (let key in queryMap) {
    let request = graphqlClient.request(queryMap[key]).then(data => {
      console.log(chalk.green(figures.tick), key)
    }).catch((error) => {
      console.log(chalk.red(figures.cross), key)
    })
    promises.push(request)
  }
  return Promise.all(promises)
}

exports.doMutation = (mutationName) => new Promise((resolve, reject) => {
  graphqlClient.request(mutationMap[mutationName]).then((data) => {
    console.log(chalk.green(figures.tick), mutationName)
    console.log(data)
    resolve(data)
  }, (error) => {
    console.log(chalk.red(figures.cross), mutationName)
    console.log(chalk.bgRed(error))
    reject(error)
  })
})

exports.doAllMutations = () => {
  var promises = []
  for (let key in mutationMap) {
    let request = graphqlClient.request(mutationMap[key]).then(data => {
      console.log(chalk.green(figures.tick), key)
    }).catch((error) => {
      console.log(chalk.red(figures.cross), key)
    })
    promises.push(request)
  }
  return Promise.all(promises)
}